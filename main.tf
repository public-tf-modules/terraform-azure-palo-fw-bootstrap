locals {
  folders = { config = var.change_config,
    content  = var.change_content,
    license  = var.change_license,
    software = var.change_software
  }
}

resource "azurerm_storage_share" "main" {
  name                 = var.name
  storage_account_name = var.storage_account_name
  quota                = 50
}

resource "null_resource" "bootstrap_dir" {
  depends_on = [azurerm_storage_share.main]
  provisioner "local-exec" {
    #    interpreter = ["PowerShell", "-Command"]
    command = <<EOT
  az storage directory create --account-name ${var.storage_account_name} --account-key ${var.storage_account_key} --share-name ${azurerm_storage_share.main.name} --name config
  az storage directory create --account-name ${var.storage_account_name} --account-key ${var.storage_account_key} --share-name ${azurerm_storage_share.main.name} --name content
  az storage directory create --account-name ${var.storage_account_name} --account-key ${var.storage_account_key} --share-name ${azurerm_storage_share.main.name} --name license
  az storage directory create --account-name ${var.storage_account_name} --account-key ${var.storage_account_key} --share-name ${azurerm_storage_share.main.name} --name software
EOT
  }
}

resource "null_resource" "upload" {
  for_each   = local.folders
  depends_on = [null_resource.bootstrap_dir]
  triggers = {
    change = each.value
  }
  provisioner "local-exec" {
    #   interpreter = ["PowerShell", "-Command"]
    command = <<EOT
  az storage file upload-batch --account-name ${var.storage_account_name} --account-key ${var.storage_account_key} --destination ${azurerm_storage_share.main.name}/${each.key}  --source ${var.bootstrap_root}/${each.key}
EOT
  }
}
