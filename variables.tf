variable "storage_account_name" {
  description = "Palo Alto Networks Storage Account Name for Bootstrap Support"
  type        = string
  default     = ""
}

variable "storage_account_key" {
  description = "Palo Alto Networks Storage Account Key for Bootstrap Support"
  type        = string
  default     = ""
}

variable "resource_group_name" {
  description = "Palo Alto Networks Resource Group Name"
  type        = string
  default     = ""
}

variable "bootstrap_root" {
  description = "Root location of bootstrap directory"
  type        = string
  default     = "./bootstrap"
}

variable "name" {}

variable "change_config" {
  default = 1
}

variable "change_content" {
  default = 1
}

variable "change_license" {
  default = 1
}

variable "change_software" {
  default = 1
}
